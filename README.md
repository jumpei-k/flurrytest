# FlurryTest
FlurryTest is a test app that allows you to see how FlurrySDK works.
It provides a simple shopping experience and this app keeps track of user's behavior by sending envet logs to Flurry.

## Feature
The users can

- move around item list and item detail page.
- mark an item as favorite or cancel it.
- take buy action.

FlurrySDK sends logs about

- user information such as push token, UUID, age, and gender
- timed events which starts when the user enters a new page and ends when exits from it
- user action

## Dependencies
- AFNetworking
- Flurry-iOS-SDK/FlurrySDK
- MBProgressHUD


//
//  ViewController.m
//  FlurryTest
//
//  Created by Jumpei Katayama on 9/11/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import "ViewController.h"
#import "Flurry.h"
#import "AFNetworking.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageName = NSStringFromClass([self class]);
    [Flurry logEvent:@"visit_homeView" timed:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [Flurry endTimedEvent:@"visit_homeView" withParameters:nil];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [Flurry logEvent:@"visit_item_table_view"];
}

- (IBAction)deleteInstall:(id)sender {
    
    //        Tap favorite button first time or favorite item
    UIAlertController *alertCtl = [UIAlertController alertControllerWithTitle:@"Delete install" message:@"Do you want to delete install?" preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *installID = (NSString*)[defaults objectForKey:@"installid"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [Flurry logEvent:@"delete_installid" withParameters:@{@"installID": installID}];

        NSString *endpoint = @"https://dev.streethawk.com/v2/events/delete";
        NSDictionary *params = @{
                                 @"delete": installID
                                 };
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager.requestSerializer setValue:installID forHTTPHeaderField:@"X-Installid"];
        [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
        [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
        
        [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSLog(@"Response: %@", responseObject);
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertCtlComplete = [UIAlertController alertControllerWithTitle:@"Complete" message:@"Your installID was deleted" preferredStyle: UIAlertControllerStyleAlert];
                
                UIAlertAction *completeAction1 = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    
                }];
                [alertCtlComplete addAction:completeAction1];
                [self presentViewController:alertCtlComplete animated:YES completion:nil];

            });


        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSLog(@"%@",ErrorResponse);
        }];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        NSLog(@"cancel AlertController");
    }];
    
    [alertCtl addAction:action1];
    [alertCtl addAction:action2];
    [self presentViewController:alertCtl animated:YES completion:nil];

}

- (IBAction)crash:(id)sender {
    int *x = NULL; *x = 42;
    
}

@end

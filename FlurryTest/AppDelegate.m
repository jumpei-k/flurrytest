//
//  AppDelegate.m
//  FlurryTest
//
//  Created by Jumpei Katayama on 9/11/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import "AppDelegate.h"
#import "Flurry.h"
#import "AFNetworking.h"
@import AdSupport;


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    NSLog(@"------------------------");
    NSLog(@"| System Informations |");
    NSLog(@"------------------------");
    
    NSLog(@"operating_system => %@",[[UIDevice currentDevice] systemName]);//
    NSLog(@"device model => %@",[[UIDevice currentDevice] model]);
    NSLog(@"os version => %@",[[UIDevice currentDevice] systemVersion]);
    NSString *utcoffset = [NSString stringWithFormat:@"%ld", [[NSTimeZone localTimeZone] secondsFromGMT] / 60];
    NSLog(@"utcoffset => %@", utcoffset);

    NSLog(@"----------");
    NSLog(@"| Flurry |");
    NSLog(@"----------");
    [Flurry startSession:@"GJVDN34PRWKCBQM3TR4G"];// Required to use Flurry
    [Flurry setDebugLogEnabled:YES];

    
//    NSLog(@"%d", [[NSTimeZone localTimeZone] secondsFromGMT] / 60); // utc offset in mins
    // First launch process
    // Get installID
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"]) {
        NSLog(@"--------------------------");
        NSLog(@"This is first time launch");
        NSLog(@"--------------------------");
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
        [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
        NSLog(@"calling post");
        
        NSLog(@"----------");
        NSLog(@"| Flurry |");
        NSLog(@"----------");
        
        NSLog(@"Sending userInfo to Flurry");

        // Should be set at the first launch?
        // https://developer.yahoo.com/flurry/docs/analytics/gettingstarted/technicalquickstart/ios/#track-user-id=
        
        [Flurry setAge:30];
        NSLog(@"Flurry: Set Age to 30");
        [Flurry setGender:@"m"];
        NSLog(@"Flurry: Set Gender to m");
        // Set theUserID
        // Both [[[UIDevice currentDevice] identifierForVendor] UUIDString] and [[NSUUID UUID] UUIDString] return same value
//        NSString *userID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//        NSString *userID = [[NSUUID UUID] UUIDString];
        NSString *userID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [Flurry setUserID:userID];
//        NSLog(@"-------------------------------");
//        NSLog(@"%@", idfa);
//        NSLog(@"-------------------------------");
        NSString *idfa = [self identifierForAdvertising];
        [Flurry logEvent:@"AppleIDFA" withParameters:@{@"uuid": idfa}];
//        [Flurry logEvent:@"identifierForVendor" withParameters:@{@"uuid": [[[UIDevice currentDevice] identifierForVendor] UUIDString]}];
//        [Flurry logEvent:@"Register remote push" withParameters:@{@"token": token, @"uuid": [[[UIDevice currentDevice] identifierForVendor] UUIDString]}];

        NSLog(@"Flurry: Set userUD to %@", userID);
        
        NSLog(@"---------------");
        NSLog(@"| Street Hawk |");
        NSLog(@"---------------");
        
        [manager POST:@"https://dev.streethawk.com/v2/events/register" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Success to register install");
            NSString *installId = [(NSDictionary *)responseObject  objectForKey:@"installid"];
            [[NSUserDefaults standardUserDefaults] setValue:installId forKey:@"installid"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"installID %@ is synchronized on your device", installId);
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
            
            if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
            {
                NSLog(@"Registering notification types...");
                UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
                [application registerUserNotificationSettings:settings];
                NSLog(@"Finish Registering notification types");
                
            } else {
                // Register to receive push notification
                NSLog(@"Register to receive push notification");
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            // Get error Response
            NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSLog(@"%@",ErrorResponse);
        }];
        
        // Install Deeplink if the install has been successfully registed with StreetHawk
        //        [self deeplinkInstall];
    }
    
    return YES;
}

- (NSString *)identifierForAdvertising
{
    if([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled])
    {
        NSUUID *IDFA = [[ASIdentifierManager sharedManager] advertisingIdentifier];
        
        return [IDFA UUIDString];
    }
    
    return nil;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}


- (void) didReceiveInstallId:(NSNotificationCenter *) notification {
    NSLog(@"received notificaiton");
}


- (void) applicationWillEnterForeground:(UIApplication *)application {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *installID = (NSString*)[defaults objectForKey:@"installid"];

    NSString *endpoint = @"https://dev.streethawk.com/v2/events/foreground";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:installID forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    
    [manager POST:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success to send event log for foreground to StreetHawk");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Failed to send event log for foreground to StreetHawk");
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];

}


- (void) applicationDidEnterBackground:(UIApplication *)application {
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/background";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *installID = (NSString*)[defaults objectForKey:@"installid"];
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    // Session log for StreetHawk
    [manager.requestSerializer setValue:installID forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    
    [manager POST:endpoint parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success to send event log for background to StreetHawk");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];
}

// MARK: Notification Settings

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    // Add notification setting to a settings app
    if (notificationSettings.types != UIUserNotificationTypeNone)
    {
        NSLog(@"registered remote notfication");
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

// ==================================================================
// Called When the device registered to received remote notifications
// ==================================================================

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken is called");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"install id is %@", (NSString*)[defaults objectForKey:@"installid"]);
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"AccessToksn: %@", token);
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/update";
    
    NSDictionary *dic = @{
                          @"carrier_name": @"T-Mobile",
                          @"access_data": token,
                          @"client_version": @"1.1",
                          @"operating_system": @"ios",
                          @"os_version": @"8.0",
                          @"live": @"false",
                          @"mode": @"dev",
                          @"model": @"iPhone6+",
                          @"height": @"1920",
                          @"width": @"1080",
                          @"utc_offset": @"-420",
                          @"macaddress": @"01:23:45:67:89:ab",
                          @"sh_cuid": [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                          @"development_platform": @"native"
                          };
    
    [Flurry logEvent:@"register_remote_push" withParameters:@{@"token": token, @"uuid": [[[UIDevice currentDevice] identifierForVendor] UUIDString]}];

    NSLog(@"Sending access data to StreetHawk");
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:(NSString*)[defaults objectForKey:@"installid"] forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];

    [manager POST:endpoint parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        self.textView.text = [self.textView.text stringByAppendingString:@"\n\nResponse: \n"];
        NSLog(@"UPFATE access data: OK");
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];
    

    
}


// ==================================================================
// Called When the device received remote notifications
// ==================================================================
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"didReceiveRemoteNotification is called");
    UIApplicationState state = [application applicationState];
    
    
    if (state == UIApplicationStateActive)
    {
        
        NSLog(@"received a notification while active...");
        
        
    }
    else if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        //opened from a push notification when the app was on background
        NSLog(@"i received a notification...");
        NSLog(@"Message: %@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]);
        NSLog(@"whole data: %@",userInfo);
    }
    NSLog(@"didReceiveRemoteNotification is called");
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"i received a notification...");

}

// ==================================================================
// Called When the device failed to register remote notifications
// ==================================================================
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError is called");
    
    NSLog(@"Failed to register for remote notfication\n ERROR: %@", error.localizedDescription);
}


- (void) deeplinkInstall {
    
    NSString *endpoint = @"https://pointzi.streethawk.com/i?";
    NSDictionary *parameters = @{@"app_key": @"FlurryTest",
                                 @"installid": @"GQRYWMML3IWG1IKH",
                                 @"sh_cuid": @"GQRYWMML3IWG1IKH",
                                 @"os": @"ios",
                                 @"device": @"iPhone6",
                                 @"version": @"8",
                                 @"timezone": @"-420",
                                 @"width": @"480",
                                 @"height": @"640",
                                 };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:endpoint parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}



@end

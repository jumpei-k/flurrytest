//
//  TaggingViewController.m
//  SHSDKLess
//
//  Created by Jumpei Katayama on 9/9/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

#import "TaggingViewController.h"

#import "AFNetworking.h"
#import "Flurry.h"



@interface TaggingViewController ()
{
    NSString *installID;
}

@end

@implementation TaggingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageName = NSStringFromClass([self class]);
    self.numericTagValueTextField.delegate = self;
    self.numericTagNameTextField.delegate = self;
    self.tagValueTextField.delegate = self;
    self.tagNameTextField.delegate = self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    installID = (NSString*)[defaults objectForKey:@"installid"];
    // Do any additional setup after loading the view.
}



- (IBAction)tagNumeric:(id)sender {
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/exit";
    
    NSLog(@"---------------");
    NSLog(@"| Entered view |");
    NSLog(@"---------------");
    NSDictionary *params = @{
                             @"page": @"ArticleDetail"
                             };

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:installID forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {

        NSLog(@"Response: %@", responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];
}

- (IBAction)deleteTag:(id)sender {
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/tag";

    NSLog(@"---------------");
    NSLog(@"| Delete tag |");
    NSLog(@"---------------");
    NSDictionary *params = @{
                              @"key": self.deleteTagNameTextField.text,
                              @"delete": @"true"
                              };
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager.requestSerializer setValue:installID forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
//        NSString * taggingEvent = [NSString stringWithFormat:@"Set tag %@ with value %@", key, value];
        NSLog(@"delete tag");
        
        
//        [Flurry logEvent:@"Set tag" withParameters:@{key: value} timed:NO];// in Flurry, user go foreground
        
        //        [Flurry logEvent:taggingEvent];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];
    
}

- (IBAction)tagging:(id)sender {
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/tag";

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *installID = (NSString*)[defaults objectForKey:@"installid"];


    NSString *tagName = self.tagNameTextField.text;
    NSString *tagValue = self.tagValueTextField.text;
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSDictionary *params = @{
                             @"key": @"nickname",
                             @"string": @"hawk"
                             };
//    NSString *sessionId = [NSString stringWithFormat:@"%u", arc4random() % 10000];
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:valuesForKey options:NSJSONWritingPrettyPrinted error:&error];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];

    [manager.requestSerializer setValue:installID forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    
    self.textView.text = @"Tagging...\n";
    self.textView.text = [self.textView.text stringByAppendingString:@"Endpoint:\n"];
    self.textView.text = [self.textView.text stringByAppendingString:endpoint];
    self.textView.text = [self.textView.text stringByAppendingString:@"\n\nParameters: \n"];
    NSLog(@"parameter: { records: %@ }", params);
    
    
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
//        NSString * taggingEvent = [NSString stringWithFormat:@"Set tag %@ with value %@", key, value];
//        NSLog(@"%@", taggingEvent);
        
        
//        [Flurry logEvent:@"Set tag" withParameters:@{key: value} timed:NO];// in Flurry, user go foreground
        
        //        [Flurry logEvent:taggingEvent];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];
    
//    [self tagStringInstallForAppWithKey:tagName value:tagValue];
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-110,320,460)]; //here taken -20 for example i.e. your view will be scrolled to -20. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,320,460)];
}



- (void) tagStringInstallForAppWithKey:(NSString *) key value:(NSString *) value {
    
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/tag";
    NSLog(@"-----------");
    NSLog(@"| Tagging |");
    NSLog(@"-----------");
    
    // Get current time and convert to NSString
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
//    endpoint = [endpoint stringByAppendingString:installID];
    
    NSArray *valuesForKey = @[@{
                                  @"key": key,
                                  @"string": value,
                                  @"datetime": dateString,
                                  }];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:valuesForKey options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    [manager.requestSerializer setValue:installID forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];

    self.textView.text = @"Tagging...\n";
    self.textView.text = [self.textView.text stringByAppendingString:@"Endpoint:\n"];
    self.textView.text = [self.textView.text stringByAppendingString:endpoint];
    self.textView.text = [self.textView.text stringByAppendingString:@"\n\nParameters: \n"];
    NSLog(@"parameter: { records: %@ }", jsonString);

    
    [manager POST:endpoint parameters:@{@"records": jsonString, @"bundle_id": @"1111"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"parameter: { records: %@ }", jsonString);
        NSLog(@"Response: %@", responseObject);
        NSString * taggingEvent = [NSString stringWithFormat:@"Set tag %@ with value %@", key, value];
        NSLog(@"%@", taggingEvent);

        
        [Flurry logEvent:@"Set tag" withParameters:@{key: value} timed:NO];// in Flurry, user go foreground

//        [Flurry logEvent:taggingEvent];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];
    
}


-(void)taggingNumericWithTagName:(NSInteger *) numeric {
    // Setup parameters
    NSArray *keysForNumeric = @[@"session_id", @"numeric", @"code", @"log_id", @"created_on_client", @"key"];
    NSArray *values = [[NSArray alloc] initWithObjects:@"sessionID", @"username", 8999, 3, numeric, nil];
    NSDictionary *params = [[NSDictionary alloc] initWithObjects:values forKeys:keysForNumeric];
    NSLog(@"%@", params);
}


-(void)taggingStringWithTagName {
    
    NSString *endpoint = @"https://dev.streethawk.com/v2/installs/log?installid=";
//    NSString *installid = @"7205YS50KC8OJM36";
    
    // Get current time and convert to NSString
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    endpoint = [endpoint stringByAppendingString:(NSString*)[defaults objectForKey:@"installid"]];
    
    NSDictionary *dic = @{@"Amazon account": @"ooodsjvbiaa@gmai@gmail.com"};

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:endpoint parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Get 100% Response
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",ErrorResponse);
    }];
}


@end

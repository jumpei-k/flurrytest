//
//  ItemDetailViewController.m
//  FlurryTest
//
//  Created by Jumpei Katayama on 10/16/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import "ItemDetailViewController.h"
#import "Flurry.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"

@interface ItemDetailViewController () <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;

}

@property (weak, nonatomic) IBOutlet UILabel *labelItem;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIButton *favoriteButton;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstraint;

@end

@implementation ItemDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageName = NSStringFromClass([self class]);
    self.labelItem.text = self.itemTitle;
    self.priceLabel.text = [@"$" stringByAppendingString:self.price];
    [self.descriptionTextView setScrollEnabled:NO];
    self.descriptionTextView.text = self.desc;
    self.imageView.image = [UIImage imageNamed:self.imageName];
    
    NSString *userItemDefaultKey = [@"userItemDefaultKey_"  stringByAppendingString:self.itemTitle];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:userItemDefaultKey] == YES) {
        [self.favoriteButton setImage:[UIImage imageNamed:@"Like Filled"] forState:UIControlStateNormal];
    } else {
        [self.favoriteButton setImage:[UIImage imageNamed:@"Like"] forState:UIControlStateNormal];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [Flurry logEvent:@"visit_item_detail" withParameters:@{@"item_name": self.itemTitle, @"price": self.price} timed:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [Flurry endTimedEvent:@"visit_item_detail" withParameters:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)share:(id)sender {
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/sharelink";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:(NSString*)[defaults objectForKey:@"installid"] forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    manager.responseSerializer = responseSerializer;
    NSDictionary *params = @{
                             @"utm_campaign": @"item_share",
                             @"utm_source": @"yahoo",
                             @"utm_medium": @"cpc",
                             @"utm_content": @"text_link",
                             @"utm_term": @"running+",
                             @"scheme": @"flurrytest",
                             @"uri": [@"item/" stringByAppendingString:self.itemTitle],
                             @"default_destination_url": @"https://streethawk.com"
                             };
    
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"success");
        [Flurry logEvent:@"generate_share_url" withParameters:params];
        NSString *responseString = (NSString *)responseObject;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"urlNotification" object:nil];
        
        NSString *shareURL = responseString;
        NSLog(@"sharelink: %@", responseString);
        
        NSString *string = @"This is share url";
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIActivityViewController *activityViewController =
            [[UIActivityViewController alloc] initWithActivityItems:@[string, shareURL]
                                              applicationActivities:nil];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self presentViewController:activityViewController animated:YES completion:^{
                NSLog(@"helo");
            }];
        });

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"failed to generate shareURL");
        [Flurry logEvent:@"fail_to_generate_share_url" withParameters:params];

        NSLog(@"error object :%@", operation.responseString);
//        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"faile->%@",error);
    }];
}

- (void)sendConfirmLogToFlurry {
    [Flurry logEvent:@"confirm_buy_product" withParameters:@{@"item_name": self.itemTitle, @"price": self.price}];

    float progress = 0.0f;
    while (progress < 1.0f)
    {
        progress += 0.01f;
        HUD.progress = progress;
        usleep(20000);
    }
    sleep(0.6);
    // UIImageView is a UIKit class, you have to initialize it on the main thread
    __block UIImageView *imageView;
    dispatch_sync(dispatch_get_main_queue(), ^{
        UIImage *image = [UIImage imageNamed:@"37x-Checkmark.png"];
        imageView = [[UIImageView alloc] initWithImage:image];
    });
    HUD.customView = imageView;
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.labelText = @"Thank you";
    sleep(2);
    
}

- (IBAction)buy:(id)sender {
    NSLog(@"buy action");
    [Flurry logEvent:@"click_buy_button" withParameters:@{@"item_name": self.itemTitle, @"price": self.price}];
    
    UIAlertController *alertCtl = [UIAlertController alertControllerWithTitle:@"Buy" message:@"Are you sure?" preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        HUD.delegate = self;
        HUD.mode = MBProgressHUDModeAnnularDeterminate;
        [HUD showWhileExecuting:@selector(sendConfirmLogToFlurry) onTarget:self withObject:nil animated:YES];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        [Flurry logEvent:@"cancel_buy_product" withParameters:@{@"item_name": self.itemTitle, @"price": self.price}];
    }];
        
    [alertCtl addAction:action1];
    [alertCtl addAction:action2];
    
    [self presentViewController:alertCtl animated:YES completion:nil];
}


- (IBAction)tapFavorite:(id)sender {
    NSLog(@"tap favorite button!");
    [Flurry logEvent:@"click_favorite_button" withParameters:@{@"item_name": self.itemTitle, @"price": self.price}];
    NSString *userItemDefaultKey = [@"userItemDefaultKey_"  stringByAppendingString:self.itemTitle];

    // cancel favorite action
    if ([[NSUserDefaults standardUserDefaults] boolForKey:userItemDefaultKey] == YES) {
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        
        UIImage *image = [[UIImage imageNamed:@"Like"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        image.renderingMode = UIImageRenderingModeAlwaysTemplate;
        HUD.customView = [[UIImageView alloc] initWithImage:image];
        HUD.customView.tintColor = [UIColor colorWithRed:0.915 green:0.152 blue:0.415 alpha:1];
        // Set custom view mode
        HUD.mode = MBProgressHUDModeCustomView;
        
        HUD.delegate = self;
        HUD.labelText = @"Unfavorite";
        [self.favoriteButton setImage:[UIImage imageNamed:@"Like"] forState:UIControlStateNormal];
        
        [HUD show:YES];
        [Flurry logEvent:@"cancel_favorite_product " withParameters:@{@"item_name": self.itemTitle, @"price": self.price}];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:userItemDefaultKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [HUD hide:YES afterDelay:1];

    } else {
        HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:HUD];
        HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Like Filled"]];
        // Set custom view mode
        HUD.mode = MBProgressHUDModeCustomView;
        HUD.delegate = self;
        HUD.labelText = @"Favorite";
        HUD.customView.tintColor = [UIColor colorWithRed:0.915 green:0.152 blue:0.415 alpha:1];
        [self.favoriteButton setImage:[UIImage imageNamed:@"Like Filled"] forState:UIControlStateNormal];
        [HUD show:YES];
        [Flurry logEvent:@"mark_favorite_product" withParameters:@{@"item_name": self.itemTitle, @"price": self.price}];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:userItemDefaultKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [HUD hide:YES afterDelay:1];

    }
}

@end

//
//  CardTableViewCell.h
//  FlurryTest
//
//  Created by Jumpei Katayama on 10/29/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"

extern const float CARD_CELL_MARGIN;

@interface CardTableViewCell : UITableViewCell <ContentItemCell>

- (void) setupWithContentItem:(Item*)item forSizing:(BOOL)isSizing;
@end

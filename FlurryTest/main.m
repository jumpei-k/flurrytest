//
//  main.m
//  FlurryTest
//
//  Created by Jumpei Katayama on 9/11/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

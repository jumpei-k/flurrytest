//
//  AppDelegate.h
//  FlurryTest
//
//  Created by Jumpei Katayama on 9/11/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


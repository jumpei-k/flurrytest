//
//  ItemTableViewController.m
//  
//
//  Created by Jumpei Katayama on 10/16/15.
//
//

#import "ItemTableViewController.h"
#import "ItemDetailViewController.h"
#import "Flurry.h"
#import "AFNetworking.h"
#import "CardTableViewCell.h"


@interface ItemTableViewController ()
{
//    NSArray *items;
    NSString *shareURL;

}

@property(nonatomic, strong) NSMutableArray *items;


@end

@implementation ItemTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.items = [[NSMutableArray alloc] init];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"products" ofType:@"json"];
    NSError * error;
    NSString* fileContents =[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    if(error)
    {
        NSLog(@"Error reading file: %@",error.localizedDescription);
    }
    
    NSDictionary *json = (NSDictionary*)[NSJSONSerialization
                          JSONObjectWithData:[fileContents dataUsingEncoding:NSUTF8StringEncoding]
                          options:0 error:NULL];
    
    NSArray *products = json[@"products"];
    NSLog(@"hello");
    for (int i = 0; i < [products count]; i++) {
        NSDictionary *prod = products[i];
        Item *newItem = [[Item new] initWithDic:prod];
        [self.items addObject:newItem];
    }

    self.tableView.estimatedRowHeight = 380;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ItemDetailViewController *dest = (ItemDetailViewController*)segue.destinationViewController;
    NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    NSLog(@"%@", self.items[path.row]);
    [Flurry logEvent:@"visist_item_detail_page"  withParameters:@{@"item": self.items[path.row]} timed:YES];
    Item *item = self.items[path.row];
    dest.itemTitle = item.title;
    dest.price = item.price;
    dest.imageName = item.imageName;
    dest.desc = item.desc;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.items count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellType = @"CardCell";
    CardTableViewCell *cardCell = [tableView dequeueReusableCellWithIdentifier:cellType];
    [cardCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (!cardCell) {
        [self.tableView registerNib:[UINib nibWithNibName:@"CardTableViewCell" bundle:nil] forCellReuseIdentifier:@"CardCell"];
        cardCell = [tableView dequeueReusableCellWithIdentifier:cellType];
    }
    id object = [self.items objectAtIndex:indexPath.row];

    if ([object isKindOfClass:[Item class]]) {
        Item *item = (Item*) object;
        [cardCell setupWithContentItem:item];
    }
    
    return cardCell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self performSegueWithIdentifier:@"to detail" sender:self];
    
}


@end

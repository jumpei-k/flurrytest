//
//  UItem.h
//  FlurryTest
//
//  Created by Jumpei Katayama on 10/29/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Item;

// A ContentItemCell is a View that can display a ContentItem
@protocol ContentItemCell <NSObject>
- (void) setupWithContentItem:(Item*)item;
@end

@interface Item : NSObject

//@property (nonatomic, strong) NSString *source;
@property (nonatomic, strong) NSString *headline;
//@property (nonatomic, strong) NSString *caption;
//@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *imageName;
@property (nonatomic, strong) NSString *desc;
//@property (nonatomic, strong) NSDate *date;
//@property (nonatomic, strong) NSArray *tags;
//@property (nonatomic, strong) UIView *videoContainer;
@property (nonatomic,strong) NSString *callToAction;
@property (nonatomic) Boolean isVideoAd;

//@property (nonatomic, strong) FlurryAdNative *ad;
//

-(instancetype)initWithDic:(NSDictionary*)dic;
//-(instancetype)initWithAd:(FlurryAdNative*)ad;
//-(BOOL) isAd;
//-(BOOL) isVideoAd;
-(UIImage*) displayImage;

@end

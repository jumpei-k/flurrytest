//
//  ShareLinkViewController.h
//  FlurryTest
//
//  Created by Jumpei Katayama on 9/24/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface ShareLinkViewController : BaseViewController
@property (weak, nonatomic) NSString *sharelink;
@property (weak, nonatomic) IBOutlet UILabel *labelShareLink;

@end

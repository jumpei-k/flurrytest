//
//  TaggingViewController.h
//  SHSDKLess
//
//  Created by Jumpei Katayama on 9/9/15.
//  Copyright (c) 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TaggingViewController : BaseViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *tagNameTextField;

@property (weak, nonatomic) IBOutlet UITextField *tagValueTextField;
@property (weak, nonatomic) IBOutlet UITextField *numericTagNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *numericTagValueTextField;
@property (weak, nonatomic) IBOutlet UITextField *deleteTagNameTextField;


@end

//
//  ItemDetailViewController.h
//  FlurryTest
//
//  Created by Jumpei Katayama on 10/16/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface ItemDetailViewController : BaseViewController
@property (weak, nonatomic) NSString *itemTitle;
@property (weak, nonatomic) NSString *price;
@property (weak, nonatomic) NSString *imageName;
@property (weak, nonatomic) NSString *desc;

@end

//
//  BaseViewController.m
//  FlurryTest
//
//  Created by Jumpei Katayama on 10/5/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import "BaseViewController.h"
#import "AFNetworking.h"


@interface BaseViewController ()
{
    NSString *installid;
}
@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    installid = (NSString*)[defaults objectForKey:@"installid"];
    NSLog(@"%@", installid);
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSDictionary *params = @{
                             @"page": self.pageName
                             };
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/enter";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:installid forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Send event log for enter: %@ => OK",self.pageName);

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];
    NSLog(@"entered: %@", self.pageName);

}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/exit";
    NSDictionary *params = @{
                             @"page": self.pageName
                             };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:installid forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Send event log for exsit: %@ => OK",self.pageName);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];
    
    NSLog(@"exited: %@", self.pageName);

}


@end

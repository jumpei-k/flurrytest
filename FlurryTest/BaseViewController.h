//
//  BaseViewController.h
//  FlurryTest
//
//  Created by Jumpei Katayama on 10/5/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property(nonatomic, strong) NSString *pageName;

@end

//
//  ShareLinkViewController.m
//  FlurryTest
//
//  Created by Jumpei Katayama on 9/24/15.
//  Copyright © 2015 Jumpei Katayama. All rights reserved.
//

#import "ShareLinkViewController.h"
#import "AFNetworking.h"

@interface ShareLinkViewController ()
{
    NSString *shareURL;
}

@end

@implementation ShareLinkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageName = NSStringFromClass([self class]);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveShareURL:) name:@"urlNotification" object:nil];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    NSString *endpoint = @"https://dev.streethawk.com/v2/events/sharelink";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:(NSString*)[defaults objectForKey:@"installid"] forHTTPHeaderField:@"X-Installid"];
    [manager.requestSerializer setValue:@"FlurryTest" forHTTPHeaderField:@"X-App-Key"];
    [manager.requestSerializer setValue:@"evtJEcEqQPhnZeOZTCbDej4vbGhojSQ5l" forHTTPHeaderField:@"X-Event-Key"];
    
    NSDictionary *params = @{
                              @"utm_campaign": @"spring_sale",
                              @"utm_source": @"yahoo",
                              @"utm_medium": @"cpc",
                              @"utm_content": @"text_link",
                              @"utm_term": @"running+",
                              @"scheme": @"fb",
                              @"uri": @"profile/12345",
                              @"default_destination_url": @"http://yahoo.com"
                              };
    
    [manager POST:endpoint parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"success");
        NSString *responseString = (NSString *)responseObject;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"urlNotification" object:nil];
        
        shareURL = responseString;

        NSLog(@"sharelink: %@", responseString);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"failed to generate shareURL");
        NSLog(@"error object :%@", operation);
        NSString* ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
        NSLog(@"%@",ErrorResponse);
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) receiveShareURL:(NSNotification*)notification {
    NSLog(@"@", notification);
}


- (IBAction)dimiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
